#!/bin/bash

function _installFlexgetIntor() {
  echo "FlexGet will now be installed." >>"${log}" 2>&1;
  echo "This process may take up to 2 minutes." >>"${log}" 2>&1;
  echo "Please wait until install is completed." >>"${log}" 2>&1;
  # output to box
  echo "FlexGet will now be installed."
  echo "This process may take up to 2 minutes."
  echo "Please wait until install is completed."
  echo
}

function _installFlexget1() {
  pip3 install flexget >> ${log} 2>&1
  touch /install/.flexget.lock
}

function _installFlexget2() {
  mkdir /home/${username}/.config/flexget
  cat > /home/${username}/.config/flexget/config.yml <<FLEXGETCONF
  web_server:
    bind: 127.0.0.1
    port: 5291
    web_ui: yes
    base_url: /flexget
    run_v2: yes

  tasks:
    example:
      priority: 2
      rss: http://example.com/rss/rss.xml
FLEXGETCONF
  chown -R "${username}":"${username}" /home/${username}/.config/flexget
}

function _installFlexget3() {
  cat > /etc/systemd/system/flexget@.service <<FLEXGET
  [Unit]
  Description=Flexget Daemon
  After=network.target

  [Service]
  Type=simple
  User=%I
  Group=%I
  UMask=000
  WorkingDirectory=/home/%I/.config/flexget/
  ExecStart=/usr/local/bin/flexget -c /home/%I/.config/flexget/config.yml daemon start
  ExecStop=/usr/local/bin/flexget -c /home/%I/.config/flexget/config.yml daemon stop
  ExecReload=/usr/local/bin/flexget -c /home/%I/.config/flexget/config.yml daemon reload

  [Install]
  WantedBy=multi-user.target
FLEXGET
  systemctl enable --now flexget@${username} >> ${log} 2>&1
  sleep 10
}

function _setFlexgetpassword() {
  echo -e "Enter FlexGet user password"
  read -e password
  flexget -c /home/${username}/.config/flexget/config.yml web passwd $password
}

function _installFlexget4() {
  if [[ -f /install/.nginx.lock ]]; then
  sleep 10
  cat > /etc/nginx/apps/flexget.conf <<FLEXGETPROXY
  location /flexget/ {
  include /etc/nginx/snippets/proxy.conf;
  proxy_pass http://127.0.0.1:5291/flexget/;
}
FLEXGETPROXY
  systemctl reload nginx
  fi
}


function _installFlexget5() {
  echo "FlexGet Install Complete!" >>"${log}" 2>&1;
  echo >>"${log}" 2>&1;
  echo >>"${log}" 2>&1;
  echo "Close this dialog box to refresh your browser" >>"${log}" 2>&1;
}

function _installFlexget6() {
  exit
}

if [[ -f /tmp/.install.lock ]]; then
  log="/root/logs/install.log"
else
  log="/root/logs/swizzin.log"
fi
username=$(cut -d: -f1 < /root/.master.info)

_installFlexgetIntor
_installFlexget1
echo "Adding config file ... " >>"${log}" 2>&1;_installFlexget2
echo "Setting up FlexGet as a service and enabling ... " >>"${log}" 2>&1;_installFlexget3
echo "Setting up FlexGet revert proxy ... " >>"${log}" 2>&1;_installFlexget4
_setFlexgetpassword
_installFlexget5
_installFlexget6
