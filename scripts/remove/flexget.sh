#!/bin/bash
username=$(cut -d: -f1 < /root/.master.info)
local_setup=/etc/QuickBox/setup/

function _removeFlexget() {
  systemctl stop flexget@${username}
  systemctl disable flexget@${username}
  sudo pip3 uninstall -y flexget >/dev/null 2>&1
  sudo apt-get -y autoremove >/dev/null 2>&1
  rm -f /etc/nginx/apps/flexget.conf
  rm -rf /home/${username}/.config/flexget
  sudo rm /install/.flexget.lock
  systemctl reload nginx
}

 _removeFlexget
