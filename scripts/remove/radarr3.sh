#!/bin/sh
username=$(cut -d: -f1 < /root/.master.info)
systemctl stop radarr@${username}.service
systemctl disable radarr@${username}.service
rm -rf /etc/systemd/system/radarr@.service
rm -rf /opt/Radarr
rm -rf /etc/nginx/apps/radarr.conf
rm -rf /install/.radarr3.lock
echo "Radarr uninstalled!"
