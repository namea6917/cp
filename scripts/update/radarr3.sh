#!/bin/bash

echo "Radarr will now be updated now."

if [[ -f /install/.radarr3.lock ]]; then
  username=$(cut -d: -f1 < /root/.master.info)
  systemctl stop radarr@${username}.service
  sleep 5
  rm -rf /opt/Radarr
  mkdir /opt/Radarr
  git clone https://gitlab.com/swizzin/radarr-v3.git /opt/Radarr >/dev/null 2>&1
  sleep 5
  chown -R ${username}:${username} /opt/Radarr/
  chmod 700 /opt/Radarr/Radarr
  systemctl start radarr@${username}.service
fi
